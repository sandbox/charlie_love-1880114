<?php //body include file 
/* DropTilesPHP
By Charlie Love, Drop Design
For Glew.org.uk

based on C# DropTiles developed Omar Al Zabir (http://glo.li/W4g66b)
Version 0.1

*/
?>
<div id="body" class="unselectable">
<div id="navbar" class="navbar navbar-fixed-top">
<div class="navbar-inner">
<div class="container-fluid">
<a class="pull-left" style="margin-top: 7px; margin-right: 5px;" href="">
<img src="<?php echo '/sites/all/themes/glewtiles/';?>img/logosml.png" style="max-height: 25px;" />
</a>
<h1><a class="brand" href="?">Tiles</a></h1>
<div class="nav-collapse">
<ul class="nav">
<li><a class="active" href="?"><i class="icon-th-large"></i>Dashboard</a></li>
<li><a href="AppLibrary"><i class="icon-shopping-cart"></i>Apps</a></li>
<li>
<form id="googleForm" class="navbar-search pull-left" action="http://www.google.com/search" target="_blank">
<input id="googleSearchText" type="text" class="search-query span2" name="q" placeholder="Google">
</form>
</li>
</ul>
<ul class="nav pull-right">
<?php /* comment out this line of script as not rquired?
<%--<li><a href="javascript:fullscreen()"><i class="icon-facetime-video"></i>Go Fullscreen</a></li>--%>
                            */ ?>
                            <li><a href="#" onClick="reset_dashboard();"><i class="icon-refresh"></i>Reset</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-tint"></i>Theme<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" onclick="ui.switchTheme('theme-green')">Green</a></li>
                                    <li><a href="#" onclick="ui.switchTheme('theme-white')">White</a></li>
                                    <li><a href="#" onclick="ui.switchTheme('theme-Bloom')">Bloom</a></li>                                    
                                </ul>
                            </li>                            
                            <li data-bind="if: user().isAnonymous"><a onclick="ui.login()" href="#login"><i class="icon-user"></i>Login</a></li>
                            <?php 
                            	//note: need to redirect this to the SimpleSAMLphp login eventually 
                            ?>
                            <li data-bind="if: !user().isAnonymous"><a href="user/logout"><i class="icon-user"></i>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" style="visibility: hidden">
            <div id="start" data-bind="text: title"></div>
            <div id="user" data-bind="with: user" onclick="ui.settings()">
                <div id="name">
                    <div id="firstname" data-bind="text: firstName">Charlie</div>
                    <div id="lastname" data-bind="text: lastName">Love</div>
                </div>
                <div id="photo">
                    <img src="<?php echo '/sites/all/themes/glewtiles/';?>img/User No-Frame.png" data-bind="attr: {src: photo}" width="40" height="40" />
                </div>
            </div>
            <div id="browser_incompatible" class="alert">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Warning!</strong>
                Your browser is incompatible with Droptiles. Please use Internet Explorer 9+, Chrome, Firefox or Safari.
            </div>
            <div id="CombinedScriptAlert" class="alert">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Warning!</strong>
                Combined javascript files are outdated. 
                Please retun the js\Combine.bat file. 
                Otherwise it won't work when you will deploy on a server.
            </div>
            <div id="metro-sections-container" class="metro">
                <div id="trash" class="trashcan">
                    <img src="/sites/all/themes/glewtiles/img/Trashcan.png" width="64" height="64" />
                </div>
                <div class="metro-sections" data-bind="foreach: sections">
                    <div class="metro-section" data-bind="attr: {id : uniqueId}, foreach: sortedTiles">
                        <div data-bind="attr: { id: uniqueId, 'class': tileClasses }">
                            <!-- ko if: tileImage -->
                            <div class="tile-image">
                                <img data-bind="attr: { src: tileImage }" src="<?php echo '/sites/all/themes/glewtiles/';?>img/Internet%20Explorer.png" />
                            </div>
                            <!-- /ko -->
                            <!-- ko if: iconSrc -->
                            <!-- ko if: slides().length == 0 -->
                            <div data-bind="attr: { 'class': iconClasses }">
                                <img data-bind="attr: { src: iconSrc }" src="<?php echo '/sites/all/themes/glewtiles/';?>img/Internet%20Explorer.png" />
                            </div>
                            <!-- /ko -->
                            <!-- /ko -->
                            <div data-bind="foreach: slides">
                                <div class="tile-content-main">
                                    <div data-bind="html: $data">
                                    </div>
                                </div>
                            </div>
                            <!-- ko if: label -->
                            <span class="tile-label" data-bind="html: label">Label</span>
                            <!-- /ko -->
                            <!-- ko if: counter -->
                            <span class="tile-counter" data-bind="html: counter">10</span>
                            <!-- /ko -->
                            <!-- ko if: subContent -->
                            <div data-bind="attr: { 'class': subContentClasses }, html: subContent">
                                subContent
                            </div>
                            <!-- /ko -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>