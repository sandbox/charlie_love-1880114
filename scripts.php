<?php  //scripts include file 
/* DropTilesPHP
 By Charlie Love, Drop Design
For Glew.org.uk

based on C# DropTiles developed Omar Al Zabir (http://glo.li/W4g66b)
Version 0.1

*/
?>
<!-- Copyright 2012 Charlie Love / based on Code by Omar Al Zabir -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<?php
//if we are running on the local server
require_once 'defaultClass.php';
$default = new _Default();
$default->page_Load(); //load the page start cookie
$testserver = array('localhost', '127.0.0.1');
if(in_array($_SERVER['HTTP_HOST'], $testserver))
{
	?>
    	<!-- 
        If you change any of the below javascript files, make sure you run the Combine.bat
        file in the /js folder to generate the CombinedDashboard.js file again. And then don't
        forget to update the ?v=14#. Otherwise user's will have cached copies in their browser
        and won't get the newly deployed file. -->
    	<script type="text/javascript" src="/sites/all/themes/glewtiles/js/TheCore.js?v=14"></script>
    	<script type="text/javascript" src="/sites/all/themes/glewtiles/tiles/tiles.js?v=14"></script>
    	<script type="text/javascript" src="/sites/all/themes/glewtiles/js/Dashboard.js?v=14"></script>
	<?php 
	} else {
		?>    
    	<script type="text/javascript" src="<?php echo '/sites/all/themes/glewtiles/';?>js/CombinedDashboard.js?v=14"></script>
    	<?php
    } 
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            <?php echo $default->getAlerts(); ?>
            }); 
    </script>
    <script type="text/javascript">
    function reset_dashboard() {
        document.cookie = encodeURIComponent("p") + "=deleted; expires=" + new Date(0).toUTCString();
        //window.location.reload();
    }
    </script>
    