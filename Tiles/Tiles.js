﻿// Copyright Charlie Love 2012 based on code Copyright 2012 Omar AL Zabir
// Part of Glew Droptiles project.
// This file holds the definition of tiles and which tiles appear by default 
// to new visitors. 


// The default tile setup offered to new users.
window.DefaultTiles = [
    {
        name :"Section1",
        tiles: [
           { id: "email", name: "email" },      //Glew/Google Mail
           { id: "calendar", name: "calendar" }, //Glew/Google Calendar              
           { id: "wordpresssml", name: "wordpress" },	  //feed from your Glew Blogs
           { id: "reader", name: "reader" },     //Glew RSS reader 
           { id: "moodlesml", name: "moodle" }, //small moodle icon linked to Glew Moodle      
           { id: "maharasml", name: "mahara" },	 //small mahara icon
           { id: "survey", name: "limesurvey" },	 //small lime survey  
           { id: "eportfolio", name: "eportfolioblogs" },	 //small lime survey  
           { id: "wiki", name: "wiki" }, 
           { id: "youtube", name: "youtube" }, 
           { id: "feature", name: "feature" }      
        ]
    },
    {
        name: "Section2",
        tiles: [
           { id: "news", name: "news" },    
           { id: "video", name: "video" },
           { id: "weather", name: "weather" },	       
           { id: "wikipedia", name: "wikipedia" },           
           { id: "flickr", name:"flickr" },           
           { id: "maps", name: "maps" }//,
           //{ id: "dynamicTile1", name: "dynamicTile"}
           ]
    }
];


// Convert it to a serialized string
//replaced _ with jQuery - not sure if correct CL
window.DefaultTiles = jQuery.map(window.DefaultTiles, function (section) {
    return "" + section.name + "~" + (jQuery.map(section.tiles, function (tile) {
        return "" + tile.id + "," + tile.name;
    })).join(".");
}).join("|");
        

// Definition of the tiles, their default values.
window.TileBuilders = {

    //BBC Weather Tile
	weather: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "weather",
            color: "bg-color-blue",
            label: "Weather",
            appTitle: "Weather App",
            appUrl: "http://www.bbc.co.uk/weather/",
            size: "tile-double",
            scriptSrc: ["/sites/all/themes/glewtiles/Tiles/weather/jQuery.simpleWeather.js", "/sites/all/themes/glewtiles/Tiles/weather/weather.js"],
            cssSrc: ["/sites/all/themes/glewtiles/Tiles/weather/weather.css"],
            initFunc: "load_weather",
            initParams: {
                location: 'Edinburgh, UK'
            }
        };
    },

    //small (single tile) Glew+ Wallwisher
    wallwisher: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "wallwisher",
            iconSrc: "/sites/all/themes/glewtiles/img/wallwisher.png",
            label: "Wallwisher",
            color: "bg-color-greenDark",
            appUrl: "http://wallwisher.com"
        };
    },         
    
    //small (single tile) Glew+ Screenr
    screenr: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "screenr",
            iconSrc: "/sites/all/themes/glewtiles/img/screenr.png",
            label: "Screenr",
            color: "bg-color-blue",
            appUrl: "http://www.screenr.com/"
        };
    },    
    
    //small (single tile) Glew+ Flickr
    flickrglew: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "flickrglew",
            iconSrc: "/sites/all/themes/glewtiles/img/Flickr alt 1.png",
            label: "Flickr",
            color: "bg-color-green",
            appUrl: "http://flickr.com"
        };
    },        
    
    //small (single tile) Glew+ Pixton
    pixton: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "pixton",
            iconSrc: "/sites/all/themes/glewtiles/img/pixton.png",
            label: "Pixton",
            color: "bg-color-purple",
            appUrl: "http://http://www.pixton.com/uk/",
        };
    },   
    
    //small (single tile) Glew+ GoAnimate
    goanimate: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "goanimate",
            iconSrc: "/sites/all/themes/glewtiles/img/goanimate.png",
            label: "GoAnimate",
            color: "bg-color-orange",
            appUrl: "http://goanimate.com/"
        };
    },       
    
    
    //small (single tile) Google Sites
    sites: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "sites",
            iconSrc: "/sites/all/themes/glewtiles/img/sites.png",
            label: "Sites",
            color: "bg-color-red",
            appUrl: "http://sites.glew.org.uk",
            appInNewWindow: true
        };
    },
    
    //small (single tile) Google Drive icon
    gdrive: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "gdrive",
            iconSrc: "/sites/all/themes/glewtiles/img/gdrive.png",
            label: "Google Drive",
            color: "bg-color-blue",
            appUrl: "http://docs.glew.org.uk",
            appInNewWindow: true
        };
    },
    
    //small (single tile) Glo.li icon
    gloli: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "gloli",
            iconSrc: "/sites/all/themes/glewtiles/img/gloli.png",
            label: "Shorten Links",
            color: "bg-color-organge",
            appUrl: "http://www.glo.li",
            appInNewWindow: true
        };
    },

    
    //small (single tile) Calendar icon for Glew
    calendar: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "calendar",
            iconSrc: "/sites/all/themes/glewtiles/img/Calendar.png",
            label: "Calendar",
            color: "bg-color-green",
            appUrl: "http://calendar.glew.org.uk"
        };
    },
    
    //small (single tile) MediaWiki icon for Glew
    wiki: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "wiki",
            iconSrc: "/sites/all/themes/glewtiles/img/mediawiki.png",
            label: "Wiki",
            color: "bg-color-orange",
            appUrl: "http://wiki.glew.org.uk/"
        };
    },
    
    
    //small (single tile) Calendar icon for Glew
    eportfolioblogs: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "eportfolioblogs",
            iconSrc: "/sites/all/themes/glewtiles/img/eportfolioblogs.png",
            label: "ePortfolio",
            color: "bg-color-red",
            appUrl: "https://folio.glew.org.uk/wp-login.php",
            appInNewWindow: true
        };
    },
    
    //small (single tile) LimeSurvey icon for Glew
    limesurvey: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "limesurvey",
            iconSrc: "/sites/all/themes/glewtiles/img/limesurvey.png",
            label: "Lime Survey",
            color: "bg-color-yellow",
            appUrl: "http://survey.glew.org.uk/"
        };
    },
    
    
    
    //small (single tile) Mahara icon for Glew
    mahara: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "mahara",
            iconSrc: "/sites/all/themes/glewtiles/img/maharasml.png",
            label: "Mahara ePortfolio",
            color: "bg-color-green",
            appUrl: "http://mahara.glew.org.uk"
        };
    },
    
    
    
    //Google Maps Tile
    maps: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "maps",
            color: "bg-color-purple",
            label: "Maps",
            appTitle: "Maps",
            appUrl: "http://maps.google.com/",
            iconSrc: "/sites/all/themes/glewtiles/img/Google Maps.png",
            appInNewWindow: true
        };
    },

    //small (single tile) Gmail icon for Glew
    email: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "email",
            iconSrc: "/sites/all/themes/glewtiles/img/Gmail Alt.png",
            label: "Gmail",
            color: "bg-color-pink",
            appUrl: "http://mail.glew.org.uk",
            appInNewWindow: true
        };
    },
    
    //small (single tile) Wordpress
    wordpress: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name:"wordpress",
            iconSrc: "/sites/all/themes/glewtiles/img/wordpress.png",
            label: "Wordpress Blog",
            color: "bg-color-green",
            appUrl: "http://blogs.glew.org.uk/wp-login.php",
            appInNewWindow: true	
        };
    },
    
    
    //small (single tile) Moodle icon for Glew
    moodle: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "moodle",
            iconSrc: "/sites/all/themes/glewtiles/img/moodle-sml.png",
            label: "Moodle",
            appUrl: "http://moodle.glew.org.uk/auth/saml/index.php",
            appInNewWindow: true	 
        };
    },    
    

    video: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "ie",
            size: "tile-double",
            color: "bg-color-darken",
            iconSrc: "/sites/all/themes/glewtiles/img/Youtube.png",
            slides: ['<iframe width="310" height="174" src="http://youtube.com/embed/g4iD-9GSW-0" frameborder="0" allowfullscreen=""></iframe>']
        };
    },

    flickr: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "flickr",
            iconSrc: "/sites/all/themes/glewtiles/img/Flickr alt 1.png",
            label: "Flickr",
            size: "tile-double tile-double-vertical",
            color: "bg-color-darken",
            appUrl: "/sites/all/themes/glewtiles/Tiles/Flickr/App/FlickrApp.html",
            cssSrc: ["/sites/all/themes/glewtiles/Tiles/flickr/flickr.css"],
            scriptSrc: ["/sites/all/themes/glewtiles/Tiles/flickr/flickr.js?v=1"],
            //scriptSrc: ["/sites/all/themes/glewtiles/Tiles/flickr/flickr_interesting.js"],
            //cssSrc: ["/sites/all/themes/glewtiles/Tiles/flickr/flickr_interesting.css"],            
            initFunc: "flickr_load"
        };
    },



    youtube: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "email",
            iconSrc: "/sites/all/themes/glewtiles/img/Youtube.png",
            label: "Youtube",
            color: "bg-color-darken",
            appUrl: "http://www.youtube.com/",
            appInNewWindow: true
        };
    },

    wikipedia: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "wikipedia",
            tileImage: "/sites/all/themes/glewtiles/img/Wikipedia alt 1.png",
            label: "Wikipedia",
            color: "bg-color-green",
            appIcon: "/sites/all/themes/glewtiles/img/Wikipedia alt 1.png",
            appUrl: "http://www.wikipedia.org"
        };
    },


    news: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "news",
            color: "bg-color-pink",
            size: "tile-double",
            appUrl: "http://www.bbc.co.uk/news/world/",
            scriptSrc: ["/sites/all/themes/glewtiles/Tiles/news/news.js?v=1"],
            cssSrc: ["/sites/all/themes/glewtiles/Tiles/news/news.css?v=1"],
            initFunc: "load_news",
            initParams: { url: "http://feeds.bbci.co.uk/news/world/rss.xml" }
        };
    },

    feature: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "feature",
            color: "bg-color-green",
            size: "tile-double",
            appUrl: "http://oazabir.github.com/Droptiles/",
            slidesFrom: ["tiles/features/feature1.html",
                "tiles/features/feature2.html",
                "tiles/features/feature3.html"],
            cssSrc: ["tiles/features/features.css"]
        };
    },

    howto: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "feature",
            color: "bg-color-blue",
            size: "tile-triple tile-triple-vertical",
            appUrl: "http://oazabir.github.com/Droptiles/",
            slidesFrom: ["tiles/features/howto.html?2"]
        };
    },

    dynamicTile: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "dynamicTile",
            color: "bg-color-darkBlue",
            size: "tile-triple tile-double-vertical",
            label: "Server side Tile in ASP.NET",
            slidesFrom: ["/sites/all/themes/glewtiles/Tiles/DynamicTile/DynamicTile.aspx"]
            //cssSrc: ["/sites/all/themes/glewtiles/Tiles/DynamicTile/visualize.css"],
            //scriptSrc: ["/sites/all/themes/glewtiles/Tiles/DynamicTile/tablechart.js",
            //    "/sites/all/themes/glewtiles/Tiles/DynamicTile/DynamicTile.js"],
            //initFunc: "load_dynamic"
        }
    },
    //small (single tile) Calendar icon for Glew
    calendar: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "calendar",
            iconSrc: "/sites/all/themes/glewtiles/img/Calendar.png",
            label: "Calendar",
            color: "bg-color-green",
            appUrl: "http://calendar.glew.org.uk",
        };
    },
    
    skydrive: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "skydrive"
        };
    },
    
    facebook: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "facebook",
            iconSrc: "/sites/all/themes/glewtiles/img/Facebook alt 3.png",
            label: "Facebook",
            color: "bg-color-blue",
            appUrl: "http://www.facebook.com/",
            appInNewWindow: true,
        };
    },
    
    angrybirds: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "angrybirds",
            tileImage: "/sites/all/themes/glewtiles/img/AppStore/Angrybirds.png",
            size: 'tile-double tile-double-vertical'
        };
    },
    
    cutTheRope: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "cutTheRope",
            size: 'tile-double',
            tileImage: "/sites/all/themes/glewtiles/img/AppStore/CutTheRope.png",
            subContent: "Feed the cute thing and win stars"
        };
    },

    
    reader: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "reader",
            color: "bg-color-red",
            label: "News Reader",
            iconSrc: '/sites/all/themes/glewtiles/img/Google Reader.png',
            appUrl: 'RssReader'
        };
    }
        
};