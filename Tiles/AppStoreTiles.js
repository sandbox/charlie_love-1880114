﻿// Copyright 2012 Omar AL Zabir
// Part of Droptiles project.
// This file holds the definition of tiles and the tiles to show on the App Store.


// The default tile setup offered to new users.
window.AppStoreTiles = [
    {
        name: "Spotlight",
        tiles: [
           { id: "flickr1", name:"flickr" },
           { id: "facebook", name: "facebook" } /*,
           { id: "angrybirds", name: "angrybirds" } */
        ]
    },
    {
        name: "Glew Apps",
        tiles: [         
                { id: "email", name: "email" },	
                { id: "calendar", name: "calendar" },	 
                { id: "gdrive", name: "gdrive" },	 
                { id: "sites", name: "sites" },
                { id: "wordpresssml", name: "wordpress" },	  //feed from your Glew Blogs
                { id: "moodlesml", name: "moodle" }, //small moodle icon linked to Glew Moodle      
                { id: "maharasml", name: "mahara" },	 //small mahara icon
                { id: "survey", name: "limesurvey" },	 //small lime survey  
                { id: "eportfolio", name: "eportfolioblogs" },	 //small lime survey  
                { id: "wiki", name: "wiki" },
                { id: "gloli", name: "gloli" }  
        ]
    },
    
    {
    	name: "Glew Plus Apps",
    	tiles: [         
            { id: "pixton", name: "pixton" },	//pixton comic maker
            { id: "wallwisher", name: "wallwisher" },	 //wallwisher
            { id: "screenr", name: "screenr" },	 //screen instant screencasts
            { id: "goanimate", name: "goanimate" }, //goanimate
            { id: "flickrglew", name: "flickrglew" } //flickr glew plus service
        ]
    },
    
    {
        name: "Tools",
        tiles: [
           { id: "youtubeapp", name: "youtube" },
           { id: "amazonapp", name: "amazon" },
           { id: "libraryapp", name: "library" },
           { id: "newsapp", name: "news" },
           { id: "weatherapp", name: "weather" }
           
        ]
    },
   /* {
        name: "Games",
        tiles: [
           { id: "angrybirds2", name: "angrybirds" },
           { id: "cuttherope2", name: "cutTheRope" }   
        ]
    }    */
];


// Convert it to a serialized string
window.AppStoreTiles = jQuery.map(window.AppStoreTiles, function (section) {
    return "" + section.name + "~" + (jQuery.map(section.tiles, function (tile) {
        return "" + tile.id + "," + tile.name;
    })).join(".");
}).join("|");
        

// Definition of the tiles, their default values.
window.TileBuilders = {

    weather: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "weather",
            size: "tile-double tile-double-vertical",
            tileImage: 'sites/all/themes/glewtiles/img/AppStore/Weather.png',
            onClick: 'addTile("weather")'
        };
    },
   
    //small (single tile) Glew+ Flickr
    flickrglew: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "flickrglew",
            iconSrc: "sites/all/themes/glewtiles/img/Flickr alt 1.png",
            label: "Flickr",
            color: "bg-color-green",
            appUrl: "http://flickr.com"
        };
    },    
    
    //small (single tile) Glew+ Wallwisher
    wallwisher: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "wallwisher",
            iconSrc: "sites/all/themes/glewtiles/img/wallwisher.png",
            label: "Wallwisher",
            color: "bg-color-yellow",
            appUrl: "http://wallwisher.com"
        };
    },         
    
    //small (single tile) Glew+ Screenr
    screenr: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "screenr",
            iconSrc: "sites/all/themes/glewtiles/img/screenr.png",
            label: "Screenr",
            color: "bg-color-blue",
            appUrl: "http://www.screenr.com/"
        };
    },    
    
    //small (single tile) Glew+ GoAnimate
    goanimate: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "goanimate",
            iconSrc: "sites/all/themes/glewtiles/img/goanimate.png",
            label: "GoAnimate",
            color: "bg-color-orange",
            appUrl: "http://goanimate.com/"
        };
    },       
    
    //small (single tile) Glew+ Pixton
    pixton: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "pixton",
            iconSrc: "sites/all/themes/glewtiles/img/pixton.png",
            label: "Pixton",
            color: "bg-color-purple",
            appUrl: "http://www.pixton.com/uk/"
        };
    },    
    
    
    //small (single tile) Google Sites
    sites: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "sites",
            iconSrc: "sites/all/themes/glewtiles/img/sites.png",
            label: "Sites",
            color: "bg-color-red",
            appUrl: "http://sites.glew.org.uk",
            appInNewWindow: true
        };
    },
    
    //small (single tile) Glo.li icon
    gloli: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "gloli",
            iconSrc: "sites/all/themes/glewtiles/img/gloli.png",
            label: "Shorten Links",
            color: "bg-color-organge",
            appUrl: "http://www.glo.li",
            appInNewWindow: true
        };
    },


    //small (single tile) Google Drive icon
    gdrive: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "gdrive",
            iconSrc: "sites/all/themes/glewtiles/img/gdrive.png",
            label: "Google Drive",
            color: "bg-color-blue",
            appUrl: "http://docs.glew.org.uk",
            appInNewWindow: true
        };
    },
    
    
    //small (single tile) Calendar icon for Glew
    calendar: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "calendar",
            iconSrc: "sites/all/themes/glewtiles/img/Calendar.png",
            label: "Calendar",
            color: "bg-color-green",
            appUrl: "http://calendar.glew.org.uk",
            appInNewWindow: true
        };
    },
 
    //small (single tile) Gmail icon for Glew
    email: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "email",
            iconSrc: "sites/all/themes/glewtiles/img/Gmail alt.png",
            label: "Gmail",
            color: "bg-color-pink",
            appUrl: "http://mail.glew.org.uk",
            appInNewWindow: true
        };
    },
    
    //small (single tile) MediaWiki icon for Glew
    wiki: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "wiki",
            iconSrc: "sites/all/themes/glewtiles/img/mediawiki.png",
            label: "Wiki",
            color: "bg-color-orange",
            appUrl: "http://wiki.glew.org.uk/"
        };
    },
    
    
    //small (single tile) Calendar icon for Glew
    eportfolioblogs: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "eportfolioblogs",
            iconSrc: "sites/all/themes/glewtiles/img/eportfolioblogs.png",
            label: "ePortfolio",
            color: "bg-color-red",
            appUrl: "https://folio.glew.org.uk/wp-login.php",
            appInNewWindow: true
        };
    },
    
    //small (single tile) LimeSurvey icon for Glew
    limesurvey: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "limesurvey",
            iconSrc: "sites/all/themes/glewtiles/img/limesurvey.png",
            label: "Lime Survey",
            color: "bg-color-yellow",
            appUrl: "https://survey.glew.org.uk/"
        };
    },
    
    
    
    //small (single tile) Mahara icon for Glew
    mahara: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "mahara",
            iconSrc: "sites/all/themes/glewtiles/img/maharasml.png",
            label: "Mahara ePortfolio",
            color: "bg-color-green",
            appUrl: "http://mahara.glew.org.uk"
        };
    },
    
    
    
    //Google Maps Tile
    maps: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "maps",
            color: "bg-color-purple",
            label: "Maps",
            appTitle: "Maps",
            appUrl: "http://maps.google.com/",
            iconSrc: "img/Google Maps.png",
            appInNewWindow: true
        };
    },


    
    //small (single tile) Wordpress
    wordpress: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name:"wordpress",
            iconSrc: "sites/all/themes/glewtiles/img/wordpress.png",
            label: "Wordpress Blog",
            color: "bg-color-green",
            appUrl: "http://blogs.glew.org.uk/wp-login.php",
            appInNewWindow: true	
        };
    },
    
    
    //small (single tile) Moodle icon for Glew
    moodle: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "moodle",
            iconSrc: "sites/all/themes/glewtiles/img/moodle-sml.png",
            label: "Moodle",
            appUrl: "http://moodle.glew.org.uk/auth/saml/index.php",
            appInNewWindow: true	 
        };
    },       
    
    amazon: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            size: 'tile-double',
            color: 'bg-color-yellow',
            iconSrc: 'sites/all/themes/glewtiles/img/amazon.png',
            label: 'Amazon, buy anything, from anywhere',
            onClick: 'addTile("amazon")'
        };
    },

    maps: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "maps",
            onClick: 'addTile("maps")'
        };
    },


    facebook: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "facebook",
            iconSrc: "sites/all/themes/glewtiles/img/Facebook alt 3.png",
            label: "Facebook",
            color: "bg-color-blue",
            appUrl: "http://www.facebook.com/",
            appInNewWindow: true,
            onClick: 'addTile("facebook")'
        };
    },

    //small (single tile) Calendar icon for Glew
    calendar: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "calendar",
            iconSrc: "sites/all/themes/glewtiles/img/Calendar.png",
            label: "Calendar",
            color: "bg-color-green",
            appUrl: "http://calendar.glew.org.uk"
        };
    },

    library: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "library",
            iconSrc: 'sites/all/themes/glewtiles/img/Libraries.png',
            label: 'Library'
            
        };
    },

    flickr: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "flickr",
            size: 'tile-triple tile-triple-vertical',
            tileImage: 'sites/all/themes/glewtiles/img/AppStore/Flickr.png'
        };
    },

    /*email: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "email",
        };
    },*/

    youtube: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "youtube",
            tileImage: 'sites/all/themes/glewtiles/img/AppStore/YouTube.png'
        };
    },

    angrybirds: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "angrybirds",
            tileImage: "sites/all/themes/glewtiles/img/AppStore/Angrybirds.png",
            size: 'tile-double tile-double-vertical'
        };
    },

    wikipedia: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "wikipedia",
            iconSrc: "sites/all/themes/glewtiles/img/Wikipedia alt 1.png"            
        };
    },


    news: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "news",
            size: "tile-double tile-double-vertical",
            tileImage: 'sites/all/themes/glewtiles/img/AppStore/News.png'
        };
    },

    feature: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "feature"   
        };
    },

    dynamicTile: function (uniqueId) {
        return {
            niqueId: uniqueId,
            name: "dynamicTile"            
        }
    },

    cutTheRope: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "cutTheRope",
            size: 'tile-double',
            tileImage: "sites/all/themes/glewtiles/img/AppStore/CutTheRope.png",
            subContent: "Feed the cute thing and win stars"
        };
    },

    buy: function (uniqueId) {
        return {
            uniqueId: uniqueId,
            name: "buy"           
        };

    }

};