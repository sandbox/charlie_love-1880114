 <?php
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="description" content="Glew Droptiles - Metro style Live Tile enabled Web 2.0 Dashboard" />
    <meta name="author" content="Charlie Love" />
    <meta name="copyright" content="2012, Charlie Love based on Code by Omar AL Zabir (Copyright 2012)" />
    <meta name="license" content="Free for personal use." />
    <meta name="apple-mobile-web-app-capable" content="yes" /> 
    
    <title>Glewtiles</title>

    <link rel="icon" href="favicon.ico" type="image/x-icon">
<?php  
    define('__ROOT__', dirname(dirname(__FILE__)));
?>

    <link rel="icon" href="favicon.ico" type="image/x-icon">
<script type="text/javascript" src="<?php echo '/sites/all/themes/glewtiles/';?>js/TheCore.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo '/sites/all/themes/glewtiles/';?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo '/sites/all/themes/glewtiles/';?>css/Droptiles.css">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
<?php
    drupal_add_js('https://html5shim.googlecode.com/svn/trunk/html5.js');
?>
    <![endif]-->
    
    <?php 

require(__ROOT__. '/glewtiles/head.php');
    ?> 
    
</head>
<body><?
require(__ROOT__.'/glewtiles/body.php');
	//include(drupal_get_path('theme', 'glewtiles') .'/body.php');
	?>
</body>
	<?php
	$testserver = array('localhost', '127.0.0.1');
	if(in_array($_SERVER['HTTP_HOST'], $testserver)) {
    ?>
	<!-- 
    If you change any of the below javascript files, make sure you run the Combine.bat
    file in the /js folder to generate the CombinedDashboard.js file again. And then don't
    forget to update the ?v=14#. Otherwise user's will have cached copies in their browser
    and won't get the newly deployed file. -->
	
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/jQueryEnhancement.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/jQuery.MouseWheel.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/jQuery.kinetic.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/Knockout-2.1.0.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/cookie.js"></script>
<script type="text/javascript" src="vjs/bootstrap.min.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/Bootstrap-tooltip.js"></script> 
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/Bootstrap.dropdown.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/Underscore.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/jQuery.hashchange.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/TheCore.js"></script>
<script type="text/javascript" src="/sites/all/themes/glewtiles/js/User.js"></script>
<?php } else { ?>
<script type="text/javascript" src="<?php echo '/sites/all/themes/glewtiles/';?>js/Combined.js?v=14"></script>
<?php } ?>    
<script type="text/javascript">
    // Bootstrap initialization
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();        
    });
</script>
<?php 
//global user from drupal
global $user;
if (!$user->uid) { ?>
<script type="text/javascript">
    window.currentUser = new User({
        firstName: "None",
        lastName: "Anonymous",
        photo: "/sites/all/themes/glewtiles/img/User No-Frame.png",
        isAnonymous: true
    });
</script>
<?php } else { 
$user_fields = user_load($user->uid);
$firstname = $user_fields->field_firstname['und']['0']['value'];
$lastname = $user_fields->field_lastname['und']['0']['value'];


$uri = module_exists('gravatar') ? _gravatar_get_account_user_picture($user) : null;
if(@$user->picture->uri != null) {
	$uri = $user->picture->uri;
} elseif ($uri == null) {
	$uri = "/sites/all/themes/glewtiles/img/User No-Frame.png";
}


 if ($user_picture): ?>
  <div class="user-picture">
    <?php print $user_picture; ?>
  </div>
<?php endif; ?>
<script type="text/javascript">
    window.currentUser = new User({
        firstName: "<?php echo $firstname; ?>",
        lastName: "<?php echo $lastname; ?>",
        photo: "<?php echo $uri; ?>",
        isAnonymous: false
    });
</script>
<?php }

require(__ROOT__ . '/glewtiles/scripts.php');
?>	
	<script type="text/ecmascript">
        
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-33406100-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>  
</html>