﻿<?php 

//define php class to be included in default.php
//this includes three functions

// Page_load function starts a new cookie if the user is not Anonymous, this cookie is used to store tile positions and is active for 30days from login
// Is_combined function checks that the combined a javascript file is up to date with the three source .js files based on last written to date. 
// GetAlerts function shows the alert is the javascript source files are not the most recent on the dates.


global $user; //load the drupal global user variable

class _Default
{
    
   public function page_Load()
   {
        //check the user global 
        //if logged in then make the cookie!
        if ($user->uid) {
        	$expire=time()+60*60*24*30; //30 days for expiration
        	setcookie("p", $profile->tiles, $expire);
            };
    }

    function getAlerts()	//show the javscript for the alert if required
    {
        
            function IsCombinedJSOlder($filepath)
    		{
        		$jsPath = dirname(__FILE__) . $filepath; //take the path to this  file and add the path to the javascript files
      		    $allFileLastWrite = strtotime('2010-01-01'); //set an initial date back in time 
        		//for each .js file in the target directory
        		foreach (glob($jsPath . '*.js') as $file) {
        			if ($allFileLastWrite < filemtime($file)) {//if the file modified date is newer than the allFileLastWrite value then
        				$allFileLastWrite = filemtime($file);  //set the allFileLastWrite to the newest modification date.
        			}
      			  }
        
      		  $combinedFileLastWrite = filemtime(dirname(__FILE__) . "/js/Combined.js");

      			if ($combinedFileLastWrite > $allFileLastWrite ) { //if the last modified date on the combined file is newer than the seprate JS files.
      				return true;								   //return true as the combined file is up to date
   			   	} else {
   			   		return false;								   //otherwise return false as combined needs to be recreated
    		  	}
 		   }
        
        $testserver = array('localhost', '127.0.0.1');
	    if(in_array($_SERVER['HTTP_HOST'], $testserver)) //okay, are we on the local test server?
        {
            if (IsCombinedJSOlder("/js/") || IsCombinedJSOlder("/Tiles/"))  //use the function to check
            {
                return "$('#CombinedScriptAlert').show();";
            }
            else
            {
                return "$('#CombinedScriptAlert').hide();"; //no alert to show as files all up to date.
            }
        }
        else
        {
            return "$('#ßßßCombinedScriptAlert').hide();"; //no alert to show as on production server
        }
        
    }
}