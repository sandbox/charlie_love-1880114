﻿<?php
//this code produces individual Tiles based on each item retrieved in the RSS Feed
//Copyright Charlie Love 2012
//
//uses PHP's simpleXML feature
//

class RssTiles  {
   
	function buildfeed($feedurl) 
	{
		return "window.TileBuilders = {
    			double: function (uniqueId, name, data) {
            	return {
                	uniqueId: uniqueId,
               	 	name: 'double',
              	  	size: data.size || 'tile-double',
                	appUrl: 'http://feeds.bbci.co.uk/news/scotland/rss.xml',
                	slides: ['<h2><a href=\"' + data.url + '\">' + data.title + '</a></h2>' + (data.image.length > 0 ? '<img src=\"' + data.image + '\" />' : '') + '<p class=\"body\">' + data.body + '</p>']
            	};
        	}
		};"
	;}   
	
   function ProcessRequest ($context) {
   	
   	//store the urls for the RSS feeds in an array (for just now)	
   	$FeedUrls = array("http://feeds.bbci.co.uk/news/scotland/rss.xml","http://uk.news.yahoo.com/rss/education", "http://feeds.bbci.co.uk/news/video_and_audio/technology/rss.xml" );
   	
   	//store the yahoo url for adding to the media links from the RSS
   	$media = "http://search.yahoo.com/mrss/";
   	$counter = 1;
    $sections = array();
    foreach ($FeedUrls as $feedUrl)
        {        	
        	$rss = simplexml_load_file($feedUrl);
            //Read the news feed channel and title into the simplified $feedTitle
            $feedTitle = $rss->channel->title;
                
            //start a new array to store each of the rss feed tiles generated
            $tiles = array();
           
			//we're going to read each item from the RSS feed up to a maximum of 6 items for each feed
            $count = 0;
            foreach ($rss->channel->item as $item)
            {
				if ($count==6) {
					break;
				}
				
            	$title = $item->title;								//feed item title;
            	$title = str_replace("'","\'",$title);
            	
            	if($item->description != null) { 
					$description =(string) trim(strip_tags($item->description)); 	//need to escape this text using php!
            	} elseif ($item->text != null)  {
            		$description = (string) trim(strip_tags($item->text));
            	} else {
            		$description = ""; //nothing in feed
            	}
            	
            	$description = str_replace("'","\'",$description);
            	
				$url = $item->link;									//link to item
				//we have to do a bit of magic to pull an image out of the RSS item
				//we need to use either the thumbnail or the content to retirve the image
				//
				foreach ($item->getNameSpaces( true ) as $key => $children) {
					$thumbnail = $item->children($children);
				}
				$thumb = $thumbnail[0]->attributes();
				$image = $thumb['url'];
				
                array_push($tiles,"{ 
                    id: '" . $count . "', 
                    name:'double', 
                    data: {
                        title:'" . $title ."', 
                        body:'" . $description . "', 
                        image:'" . $image . "', 
                        url: '" . $url . "'
                    }
                }");     //write the tile using the RSS feed data
                $count++;
            }           
            array_push($sections,"
                {
                    title: '" . $feedTitle . "',
                    tiles : [" . implode(",", $tiles) . "]
                }");         
        }

        return "window.RssTiles = [" . implode(",",$sections) . "];";

    }
 
    
    function IsReusable(){
        
            return false;
    }

}
$rsstile = new RssTiles();
echo $rsstile->buildfeed("") . "\n";
echo $rsstile->ProcessRequest("") .  "\n";
echo $rsstile->buildfeed(""); 
?>