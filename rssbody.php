<?php
//RSS body include for GlewTiles
?>
    <div id="body" class="unselectable">        
        <div id="content" style="visibility: hidden">
            <div id="metro-sections-container" class="metro">
                <div id="trash" class="trashcan">
                    <img src="/sites/all/themes/glewtiles/img/Trashcan.png" width="64" height="64" />
                </div>
                <div class="metro-sections" data-bind="foreach: sections">
                    <div class="metro-section" data-bind="attr: {id : uniqueId}">
                        <div class="metro-section-title" data-bind="{text: name}"></div>
                        <!-- ko foreach: sortedTiles -->
                        <div data-bind="attr: { id: uniqueId, 'class': tileClasses }">
                            <b class="check"></b>
                            <!-- ko if: tileImage -->
                            <div class="tile-image">
                                <img data-bind="attr: { src: tileImage }" src="sites/all/themes/glewtiles/img/Internet%20Explorer.png" />
                            </div>
                            <!-- /ko -->
                            <!-- ko if: iconSrc -->
                            <!-- ko if: slides().length == 0 -->
                            <div data-bind="attr: { 'class': iconClasses }">
                                <img data-bind="attr: { src: iconSrc }" src="sites/all/themes/glewtiles/img/Internet%20Explorer.png" />
                            </div>
                            <!-- /ko -->
                            <!-- /ko -->
                            <div data-bind="foreach: slides">
                                <div class="tile-content-main">
                                    <div data-bind="html: $data">
                                    </div>
                                </div>
                            </div>
                            <!-- ko if: label -->
                            <span class="tile-label" data-bind="html: label">Label</span>
                            <!-- /ko -->
                            <!-- ko if: counter -->
                            <span class="tile-counter" data-bind="html: counter">10</span>
                            <!-- /ko -->
                            <!-- ko if: subContent -->
                            <div data-bind="attr: { 'class': subContentClasses }, html: subContent">
                                subContent
                            </div>
                            <!-- /ko -->
                        </div>
                        <!-- /ko -->
                    </div>

                </div>
                
            </div>
        </div>
    </div>